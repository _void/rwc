# rwc
rwc - analog of wc(1) written on Rust programming language
# requirements
rustc, cargo
# build
```bash
make
#install
sudo make install
```
# examples
```bash
echo "Hello world" | rwc -lw
#    1    2
```
