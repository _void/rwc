TARGET=/usr/local/bin
PROG=./target/release/rwc

all:
	cargo build --release

install:
	gzip -c rwc.1 > rwc.1.gz
	cp $(PROG) $(TARGET)
	cp rwc.1.gz /usr/share/man/man1/
	rm rwc.1.gz

uninstall:
	rm $(TARGET)/rwc
	rm /usr/share/man/man1/rwc.1.gz

clean:
	rm -rf ./target

