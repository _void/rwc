pub fn count_lines(s: &String) -> i32 {
    let mut count: i32 = 0;

    for _ in s.lines() {
        count += 1;
    }
    count
}

pub fn count_words(s: &String) -> i32 {
    let mut count: i32 = 0;
    let s = s.as_str();

    for _ in s.split_whitespace() {
        count += 1;
    }
    count
}

pub fn count_chars(s: &String) -> i32 {
    let mut count: i32 = 0;

    for _ in s.chars() {
        count += 1;
    }
    count
}

pub fn count_bytes(s: &String) -> i32 {
    let mut count: i32 = 0;
    let s = s.as_bytes();

    for _ in s.iter() {
        count += 1;
    }
    count
}
