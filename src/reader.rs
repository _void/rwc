use crate::counter::*;
use crate::Cli;
use std::fs::File;
use std::io::{read_to_string, stdin, BufReader};

pub struct Total {
    pub lines: i32,
    pub words: i32,
    pub chars: i32,
    pub bytes: i32,
}

// make string that we will print
fn make_output(o: &mut String, s: &String) {
    o.push('\t');
    o.push_str(s.as_str());
}

pub fn read_from_files(output: &mut Vec<String>, cli: &Cli, total: &mut Total) {
    for file in &cli.files {
        let f = File::open(&file).expect("Failed to open file.");
        let reader = BufReader::new(f);
        let mut out = String::new();
        let text = read_to_string(reader).expect("Failed to read file.");

        if cli.lines {
            let lines = count_lines(&text);
            make_output(&mut out, &lines.to_string());
            total.lines += lines;
        }
        if cli.words {
            let words = count_words(&text);
            make_output(&mut out, &words.to_string());
            total.words += words;
        }
        if cli.chars {
            let chars = count_chars(&text);
            make_output(&mut out, &chars.to_string());
            total.chars += chars;
        }
        if cli.bytes {
            let bytes = count_bytes(&text);
            make_output(&mut out, &bytes.to_string());
            total.bytes += bytes;
        }
        if out == "" {
            let lines = count_lines(&text);
            let words = count_words(&text);
            let bytes = count_bytes(&text);
            out = format!("\t{}\t{}\t{}", lines, words, bytes,);
            total.lines += lines;
            total.words += words;
            total.bytes += bytes;
        }

        out.push('\t');
        out.push_str(file.as_str());
        output.push(out);
    }
    let mut out: String = String::new();
    if cli.lines {
        make_output(&mut out, &total.lines.to_string());
    }
    if cli.words {
        make_output(&mut out, &total.words.to_string());
    }
    if cli.chars {
        make_output(&mut out, &total.chars.to_string());
    }
    if cli.bytes {
        make_output(&mut out, &total.bytes.to_string());
    }
    if out == "" {
        out = format!("\t{}\t{}\t{}", total.lines, total.words, total.bytes);
    }
    out.push('\t');
    out.push_str("Total");
    output.push(out);
}

pub fn read_from_stdin(output: &mut Vec<String>, cli: &Cli) {
    let reader = BufReader::new(stdin());
    let mut out = String::new();
    let text = read_to_string(reader).expect("Failed to read file.");

    if cli.lines {
        let lines = count_lines(&text);
        make_output(&mut out, &lines.to_string());
    }
    if cli.words {
        let words = count_words(&text);
        make_output(&mut out, &words.to_string());
    }
    if cli.chars {
        let chars = count_chars(&text);
        make_output(&mut out, &chars.to_string());
    }
    if cli.bytes {
        let bytes = count_bytes(&text);
        make_output(&mut out, &bytes.to_string());
    }
    if out == "" {
        out = format!(
            "\t{}\t{}\t{}",
            count_lines(&text),
            count_words(&text),
            count_bytes(&text),
        );
    }

    output.push(out);
}
