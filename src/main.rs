/* TODO: Add total sum from all files;
 */
mod counter;
mod reader;

use crate::reader::*;
use clap::Parser;

#[derive(Parser)]
#[command(about = "rwc - rust replacement of wc(1)")]
#[command(name = "rwc", author = "_void", version)]
pub struct Cli {
    #[arg(long, short)]
    lines: bool,

    #[arg(long, short)]
    words: bool,

    #[arg(long, short)]
    chars: bool,

    #[arg(long, short)]
    bytes: bool,

    /// Optional: Read from file instead standart in
    files: Vec<String>,
}

fn main() {
    let cli: Cli = Cli::parse();
    let mut total: Total = Total {
        lines: 0,
        words: 0,
        chars: 0,
        bytes: 0,
    };
    let mut output: Vec<String> = Vec::new();

    if cli.files.is_empty() {
        read_from_stdin(&mut output, &cli);
    } else {
        read_from_files(&mut output, &cli, &mut total);
    }

    for i in output {
        println!("{}", i);
    }
}
